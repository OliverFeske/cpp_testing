#include "Account.h"
#include "ExeptionClasses.h"
#include <iostream>

using std::cout;
using std::endl;

// Constructors
Account::Account(string InName, double InBalance)
	: Name{ InName }, Balance{ InBalance }
{
	if (Balance < 0.0)
		throw IllegalBalanceExeption{};
}

bool Account::Deposit(double InAmount)
{
	if (InAmount <= 0) { return false; }
	else
	{
		Balance += InAmount;
		return true;
	}
}

bool Account::Withdraw(double InAmount)
{
	if (Balance - InAmount >= 0)
	{
		Balance -= InAmount;
		return true;
	}
	else { throw InsufficientFundsException{}; }
}