#pragma once

#include "Player.h"


Player::Player(const Player& Source)
	: Name{ Source.Name }, Health{ Source.Health }, XP{ Source.XP } {}

Player::Player()
	: Player{ "Unnamed", 0, 0 } {}

Player::Player(std::string NameValue)
	: Player{ NameValue, 0, 0 } {}

Player::Player(std::string NameValue, int HealthValue, int XPValue)
	: Name{ NameValue }, Health{ HealthValue }, XP{ XPValue } {}