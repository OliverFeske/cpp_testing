#pragma once

#include <vector>
#include <string>

using std::string;
using std::vector;

class Movie
{
public:
	// Constructor
	Movie(string MovieName, string RatingName, int WatchedValue);

	string GetName() { return Name; }
	string GetRating() { return Rating; }
	int GetWatchCount() { return WatchCount; }
	void SetWatchCount(int Value);
private:
	string Name{ "" };
	string Rating{ "" };
	int WatchCount{ 0 };
};
