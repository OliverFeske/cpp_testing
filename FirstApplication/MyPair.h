#pragma once

template <typename T1, typename T2>
struct MyPair
{
	T1 First;
	T2 Second;
};