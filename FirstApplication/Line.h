#pragma once

#include "OpenShape.h"

using std::cout;
using std::endl;

class Line : public OpenShape
{
public:
	virtual ~Line() {};

	virtual void Draw() override { cout << "Drawing a line" << endl; }
	virtual void Rotate() override { cout << "Rotating a line" << endl; }
};