#pragma once

#include <string>

class Player
{
public:
	// Copy Constructor
	Player(const Player& Source);
	// Constructors
	Player();
	Player(std::string NameValue);
	Player(std::string NameValue, int HealthValue, int XPValue);
public:
	std::string Name;
	int Health;
	int XP;

	void Talk(std::string Message);
	bool IsDead();
};