#pragma once

#include "Account.h"

class SavingsAccount : public Account
{
public:
	virtual void Print(ostream& Os) const override;
	// Constructors
	SavingsAccount(string InName = DEFAULT_NAME, double InBalance = DEFAULT_BALANCE, double InInterest = DEFAULT_INTEREST_RATE);
	//Destructor
	virtual ~SavingsAccount() { cout << "Savings Account destructor" << endl; }

	virtual bool Deposit(double InAmount) override;
	virtual bool Withdraw(double InAmount) override { return Account::Withdraw(InAmount); }

protected:
	static constexpr double DEFAULT_BALANCE = 0.0;
	static constexpr double DEFAULT_INTEREST_RATE = 0.0;

	double InterestRate;

private:
	static constexpr const char* DEFAULT_NAME = "Unnamed Savings Account";
};

