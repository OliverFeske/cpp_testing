#pragma once

#include <iostream>
#include <string>

using std::string;
using std::ostream;
using std::istream;

class MyString
{
	friend ostream& operator<<(ostream& Os, const MyString& Rhs);
	friend istream& operator>>(istream& Is, MyString& Rhs);

private:
	string* Str{ nullptr };

public:
	// Constructors
	MyString();
	MyString(const string* Source);
	MyString(const MyString& Source);
	MyString(MyString&& Source) noexcept;

	// Destructor
	~MyString();

	// Overloaded operators
	MyString& operator=(const MyString& Rhs);
	MyString& operator=(MyString&& Rhs) noexcept;
	bool operator==(const MyString& Rhs) const;
	bool operator!=(const MyString& Rhs) const;
	bool operator<(const MyString& Rhs) const;
	bool operator>(const MyString& Rhs) const;

	int GetLength() const;
	const string* GetStr() const;
};

