#pragma once

#include "ClosedShape.h"

using std::cout;
using std::endl;

class Circle : public ClosedShape
{
public:
	virtual ~Circle() {};

	virtual void Draw() override { cout << "Drawing a circle" << endl; }
	virtual void Rotate() override { cout << "Rotating a circle" << endl; }
};