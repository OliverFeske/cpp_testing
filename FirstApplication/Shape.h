#pragma once

#include <iostream>

class Shape
{
private:

public:
	virtual ~Shape() {}

	virtual void Draw() = 0;
	virtual void Rotate() = 0;
};