#pragma once

#include <string>
#include "IPrintable.h"

using std::ostream;
using std::string;
using std::cout;
using std::endl;

class Account : public IPrintable
{
public:
	virtual void Print(ostream& Os) const override { Os << "Account display"; }
	// Constructors
	Account(string InName = DEFAULT_NAME, double InBalance = DEFAULT_BALANCE);
	/*
	Account(const Account& source); // Copy Constructor
	Account(Account&& source); // Move Constructor
	*/
	// Destructor
	virtual ~Account() { cout << "Account destructor" << endl; }

	virtual bool Deposit(double InAmount) = 0;
	virtual bool Withdraw(double InAmount) = 0;

protected:
	string Name;
	double Balance;

private:
	static constexpr const char* DEFAULT_NAME = "Unnamed Account";
	static constexpr double DEFAULT_BALANCE = 0.0;
};