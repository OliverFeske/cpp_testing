#include "Movies.h"
#include "Movie.h"

using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::getline;

void Movies::DefaultUpdate()
{
	DisplayMenu();
	SwitchOverInput();
}

void Movies::DisplayMenu() const
{
	cout << "=============================" << " Commands " << "=============================" << endl;
	cout << "D - Display movie list" << endl;
	cout << "A - Add a movie to the list" << endl;
	cout << "O - Add 1 to the count of a specific movie" << endl;
	cout << "X - Add a custom value to the count of a specific movie" << endl;
	cout << "Q - Quit the application" << endl;
	cout << endl;
}

void Movies::SwitchOverInput()
{
	cin >> Command;
	cout << endl;

	switch (Command)
	{
	case 'd':
	case'D':
		DisplayMovieList();
		break;
	case'a':
	case'A':
		AddMovie();
		break;
	case'o':
	case'O':
		AddWatchCountOne();
		break;
	case'x':
	case'X':
		AddWatchCount();
		break;
	case'q':
	case'Q':
		QuitApplication();
		break;
	default:
		cout << "Invalid command! Please enter a new command..." << endl;
		cout << endl;
		break;
	}
}

void Movies::DisplayMovieList() const
{
	if (MovieList.size() == 0)
	{
		cout << "There are no movies in the list yet." << endl;
		cout << endl;
		return;
	}

	for (Movie* M : MovieList)
	{
		cout << M->GetName() + " " << M->GetRating() + " " << M->GetWatchCount() << endl;
	}
	cout << endl;
}

void Movies::AddMovie()
{
	string MovieName{ "" };
	string Rating{ "" };
	int WatchAmount{ 0 };
	cout << "Please Enter the name of the movie: ";
	cin.ignore(1, '\n');
	getline(cin, MovieName);
	cout << endl;
	cout << "( Rating categories:  G, PG, PG-13, R ) " << endl;
	cout << "...now its rating: ";
	getline(cin, Rating);
	cout << endl;
	cout << "...and atlast your watchamount: ";
	cin >> WatchAmount;
	cout << endl;

	if (ValidateMovie(MovieName, Rating, WatchAmount))
	{
		Movie* NewMovie = new Movie(MovieName, Rating, WatchAmount);
		MovieList.push_back(NewMovie);
		cout << "The Movie " << MovieName << " with " << Rating << " and a watchamount of " << WatchAmount << " times got added to the list." << endl;
		cout << endl;
	}
}

void Movies::AddWatchCountOne()
{
	string MovieName{ "" };
	cout << "Please enter the name of the movie: ";
	cin.ignore(1, '\n');
	getline(cin, MovieName);
	cout << endl;
	cout << endl;

	Movie* Movie{ GetMovieWithName(MovieName) };
	if (Movie != nullptr)
	{
		Movie->SetWatchCount(1);
	}
	else
	{
		cout << "A movie with this name does not exist!" << endl;
		cout << endl;
	}
}

void Movies::AddWatchCount()
{
	string MovieName{ "" };
	int WatchCountToAdd{ 0 };
	cout << "Please enter the name of the movie: ";
	cin.ignore(1, '\n');
	getline(cin, MovieName);
	cout << "...and the watchamount to add: ";
	cin >> WatchCountToAdd;
	cout << endl;
	cout << endl;

	Movie* Movie{ GetMovieWithName(MovieName) };
	if (Movie != nullptr)
	{
		if (CheckIfIsPositive(WatchCountToAdd))
			Movie->SetWatchCount(WatchCountToAdd);
	}
	else
	{
		cout << "A movie with this name does not exist!" << endl;
		cout << endl;
	}
}

Movie* Movies::GetMovieWithName(const string& MovieName)
{
	for (Movie* Movie : MovieList)
	{
		if (MovieName == Movie->GetName())
			return Movie;
	}

	cout << "A movie with this name does not exist!" << endl;
	cout << endl;
	return nullptr;
}

bool Movies::ValidateMovie(const string& MovieName, const string& RatingName, int Value) const
{
	if (!CheckIfNameExists(MovieName) && CheckIfRatingIsCorrect(RatingName) && CheckIfIsPositive(Value))
		return true;

	return false;
}

bool Movies::CheckIfNameExists(const string& MovieName) const
{
	for (Movie* Movie : MovieList)
	{
		if (MovieName == Movie->GetName())
		{
			cout << "A movie with this name does already exist!" << endl;
			cout << endl;
			return true;
		}
	}

	return false;
}

bool Movies::CheckIfRatingIsCorrect(const string& RatingName) const
{
	for (string Name : Ratings)
	{
		if (RatingName == Name)
			return true;
	}

	cout << "The passed in rating name does not exist!" << endl;
	cout << endl;
	return false;
}

bool Movies::CheckIfIsPositive(int Value) const
{
	if (Value >= 0)
		return true;

	cout << "The watchamount must not be negative!" << endl;
	cout << endl;
	return false;
}

void Movies::QuitApplication()
{
	bIsRunning = false;
	cout << "Bye!" << endl;
	DeconstructMovies();
}

void Movies::DeconstructMovies()
{
	cout << "MOVIES destructor called" << endl;

	for (Movie* Movie : MovieList)
	{
		delete Movie;
	}

	MovieList.clear();
}