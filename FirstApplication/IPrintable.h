#pragma once

#include <iostream>

using std::ostream;

class IPrintable
{
	friend ostream& operator<<(ostream& Os, const IPrintable& obj);

public:
	virtual void Print(ostream& Os) const = 0;
	virtual ~IPrintable() = default;
};