#pragma once

#include <iostream>

using std::cout;
using std::endl;

class Test
{
public:
	// Constructors
	Test() : Data{ 0 } { cout << "\tTest constructor -" << Data << "-" << endl; }
	Test(int InData) : Data{ InData } { cout << "\tTest constructor -" << Data << "-" << endl; }
	// Destructor
	~Test() { cout << "\tTest destructor -" << Data << "-" << endl; }

	int GetData() const { return Data; }

private:
	int Data;
};