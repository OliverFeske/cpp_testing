#pragma once

#include "Shape.h"

class ClosedShape : public Shape
{
public:
	virtual ~ClosedShape() {};
};