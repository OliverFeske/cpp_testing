#pragma once

#include <iostream>

using std::endl;
using std::cout;
using std::ostream;

template <typename T, int N>
class Array
{
private:
	int Size{ N };
	T Values[N];

	friend ostream& operator<<(ostream& Os, const Array<T, N>& Arr)
	{
		Os << "[ ";
		for (const auto& Val : Arr.Values)
		{
			Os << Val << " ";
		}
		Os << "]" << endl;
		return Os;
	}

public:
	Array() = default;
	Array(T InValue)
	{
		for (auto& Item : Values)
		{
			Item = InValue;
		}
	}
	void Fill(T InValue)
	{
		for (auto& Item : Values)
		{
			Item = InValue;
		}
	}
	int GetSize() const { return Size; }
	T& operator[](int Index)
	{
		return Values[Index];
	}
};